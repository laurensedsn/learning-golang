package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var gallows = []string{
	`
  +---+
  |   |
  O   |
 /|\  |
 / \  |
      |
========= `,
	`
  +---+
  |   |
  O   |
 /|\  |
 /    | 
      |
=========`,
	`
  +---+
  |   |
  O   |
 /|\  |
      |
      |
========= `,
	`
  +---+
  |   |
  O   |
 /|   |
      |
      |
========= `,
	` 
  +---+
  |   |
  O   |
  |   |
      |
      |
========= `,
	`
  +---+
  |   |
  O   |
      |
      |
      |
========= `,
	`
  +---+
  |   |
      |
      |
      |
      |
========= `,
}

type char struct {
	letter string
	pos    int
}

var (
	dict       [][]string
	gameOver   bool
	turnsLeft  int
	wordStr    string
	wordStruct []char
	toFill     []string
	usedChar   []string
)

func loadDict() {
	f, err := os.Open("words.csv")
	if err != nil {
		log.Fatal("Unable to read file", err)
	}

	defer f.Close()

	csvReader := csv.NewReader(f)
	results, err := csvReader.ReadAll()
	if err != nil {
		log.Fatal("Unable to parse file CSV ", err)
	}

	dict = results

}

func showBoard() {
	fmt.Println("Turns left: " + strconv.Itoa(turnsLeft))
	fmt.Println(gallows[turnsLeft-1])
	fmt.Println(toFill)
	fmt.Println("")
	fmt.Println(usedChar)
}

func parseChosenWord(word string) {
	wordSplit := strings.Split(word, "")
	for i, w := range wordSplit {
		var newLett = char{letter: w, pos: i}
		wordStruct = append(wordStruct, newLett)
		toFill = append(toFill, "_")
	}
}

func addToUsedChar(move string) {
	// Adds letters, and ensures each
	// letter is only added once

	for _, c := range usedChar {
		if strings.ContainsAny(c, move) {
			return
		}
	}

	usedChar = append(usedChar, move)
}

func validateMove(move string) (string, error) {
	r, _ := regexp.Compile("[a-z]")

	if len(move) > 1 {
		return "", errors.New("You can only guess a single letter at a time")
	}

	for _, c := range usedChar {
		if strings.ContainsAny(c, move) {
			return "", errors.New("You've used this letter before")
		}
	}

	if !r.MatchString(move) || len(move) == 0 {
		return "", errors.New("Only lowercase a-z letters are accepted")
	}

	var validMove bool
	for _, w := range wordStruct {
		if strings.ContainsAny(w.letter, move) {
			validMove = true
		}
	}

	if !validMove {
		turnsLeft = turnsLeft - 1
		addToUsedChar(move)

		if turnsLeft == 0 {
			fmt.Println(gallows[0])
			fmt.Println(usedChar)
			fmt.Println("The word was " + wordStr)
			gameOver = true
			return "", errors.New("You lost")
		}

		return "", errors.New("Wrong Guess. " + string(turnsLeft) + " turns left")
	}

	return move, nil
}

func makeMove(move string) {
	for _, w := range wordStruct {
		if w.letter == move {
			toFill[w.pos] = w.letter
			addToUsedChar(move)
		}
	}
}

func validateWin(move string) bool {
	for _, w := range toFill {
		if w == "_" {
			return false
		}
	}
	return true
}

func main() {

	turnsLeft = 7

	loadDict()

	rand.Seed(time.Now().UnixNano())
	var randInt = rand.Intn(len(dict))
	fmt.Println(randInt)
	wordStr = dict[randInt][0]

	parseChosenWord(wordStr)

	for !gameOver {

		if turnsLeft == 0 {
			fmt.Println("You lose")
			gameOver = true
		}

		showBoard()
		var charChosen string

		fmt.Println("Type in a single character [a-z]:")
		fmt.Scanln(&charChosen)

		move, err := validateMove(charChosen)

		if err != nil {
			fmt.Println(err)
			continue
		}

		makeMove(move)

		hasWon := validateWin(move)

		if hasWon {
			fmt.Println("Congratulations! You won!")
			showBoard()
			gameOver = true
		}

	}

}
