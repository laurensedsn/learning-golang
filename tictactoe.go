package main

import (
	"fmt"
	"strings"
)

// Build Board
var board = [][]string{
	[]string{"_", "_", "_"},
	[]string{"_", "_", "_"},
	[]string{"_", "_", "_"},
}

func showBoard(turn string, win bool) {
	if win {
		fmt.Println(turn + " has won!")
	} else {
		fmt.Println("Tic Tac Toe (Turn for " + turn + ")")
	}
	for i := 0; i < len(board); i++ {
		fmt.Printf("%s\n", strings.Join(board[i], " "))
	}
}

func main() {
	gameOver := false
	turn := "X"

	//Main game loop
	for gameOver == false {

		showBoard(turn, false)

		var x int
		var y int

		fmt.Println("Choose your x position (1, 2, or 3)")
		fmt.Scanln(&x)

		fmt.Println("Choose your y position (1, 2, or 3)")
		fmt.Scanln(&y)

		if board[y-1][x-1] != "_" {
			fmt.Println("\nyour move was invalid, try again\n")
		} else {
			board[y-1][x-1] = turn

			var conditions = [][]string{
				[]string{board[0][0], board[0][1], board[0][2]},
				[]string{board[1][0], board[1][1], board[1][2]},
				[]string{board[2][0], board[2][1], board[2][2]},

				[]string{board[0][0], board[1][0], board[2][0]},
				[]string{board[0][1], board[1][1], board[2][1]},
				[]string{board[0][2], board[1][2], board[2][2]},

				[]string{board[0][0], board[1][1], board[2][2]},
				[]string{board[0][2], board[1][1], board[2][0]},
			}

			for _, c := range conditions {
				if c[0] == turn && c[0] == c[1] && c[1] == c[2] {
					showBoard(turn, true)
					gameOver = true
				}
			}

			if turn == "X" {
				turn = "O"
			} else {
				turn = "X"
			}
		}

		var hasBlanks bool

		for _, sub := range board {
			for _, square := range sub {
				if square == "_" {
					hasBlanks = true
				}
			}
		}

		if hasBlanks == false {
			gameOver = true
		}

	}

	fmt.Println("Game Over")
}
